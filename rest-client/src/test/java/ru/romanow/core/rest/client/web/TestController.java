package ru.romanow.core.rest.client.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.romanow.core.rest.client.model.ErrorHolder;
import ru.romanow.core.rest.client.model.ParamHolder;
import ru.romanow.core.rest.client.model.TestRequest;
import ru.romanow.core.rest.client.model.TestResponse;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by ronin on 22.01.17
 */
@RestController
public class TestController {
    private static final Logger logger = LoggerFactory.getLogger(TestController.class);

    public static final String PING_ENDPOINT = "/ping";
    public static final String PARAM_ENDPOINT = "/param";
    public static final String REQUEST_ENDPOINT = "/request";
    public static final String ERROR_BODY_ENDPOINT = "/errorBody";
    public static final String BAD_REQUEST_ENDPOINT = "/badRequest";
    public static final String RETRY_REQUEST_ENDPOINT = "/retryRequest";
    public static final String TIMEOUT_REQUEST_ENDPOINT = "/timeout";
    public static final String INTERNAL_SERVER_ERROR_ENDPOINT = "/internalServerError";

    public volatile AtomicInteger retryCount = new AtomicInteger();

    @GetMapping(value = PING_ENDPOINT)
    public String ping() {
        return "OK";
    }

    @GetMapping(value = RETRY_REQUEST_ENDPOINT)
    public ResponseEntity retry() {
        logger.info("Request [{}] iteration {}", RETRY_REQUEST_ENDPOINT, retryCount);

        if (retryCount.incrementAndGet() < 3) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException ignored) {}
        }
        return ResponseEntity.ok("OK");
    }

    @GetMapping(value = TIMEOUT_REQUEST_ENDPOINT)
    public void timeout() {
        try {
            Thread.sleep(200);
        } catch (InterruptedException ignored) {}
    }

    @GetMapping(value = PARAM_ENDPOINT)
    public ParamHolder param(@RequestParam Integer code, @RequestParam String message) {
        return new ParamHolder(code, message);
    }

    @PostMapping(value = REQUEST_ENDPOINT)
    public TestResponse testRequest(@RequestBody TestRequest request) {
        return new TestResponse(request.getData().length(), request.getMessage());
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @GetMapping(value = ERROR_BODY_ENDPOINT)
    public ErrorHolder errorBody(@RequestParam Integer code, @RequestParam String message) {
        return new ErrorHolder(code, message);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @RequestMapping(BAD_REQUEST_ENDPOINT)
    public void badRequest() {}

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @RequestMapping(INTERNAL_SERVER_ERROR_ENDPOINT)
    public void internalServerError() {}
}