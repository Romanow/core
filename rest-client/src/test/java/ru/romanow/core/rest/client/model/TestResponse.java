package ru.romanow.core.rest.client.model;

import com.google.common.base.MoreObjects;

/**
 * Created by ronin on 13.02.16
 */
public class TestResponse {
    private Integer code;
    private String message;

    public TestResponse() {}

    public TestResponse(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {

        return code;
    }

    public TestResponse setCode(Integer code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public TestResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("code", code)
                          .add("message", message)
                          .toString();
    }
}
