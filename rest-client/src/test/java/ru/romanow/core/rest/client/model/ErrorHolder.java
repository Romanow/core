package ru.romanow.core.rest.client.model;

import com.google.common.base.MoreObjects;

/**
 * Created by ronin on 22.01.17
 */
public class ErrorHolder {
    private Integer code;
    private String message;

    public ErrorHolder() {}

    public ErrorHolder(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public ErrorHolder setCode(Integer code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public ErrorHolder setMessage(String message) {
        this.message = message;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("code", code)
                          .add("message", message)
                          .toString();
    }
}
