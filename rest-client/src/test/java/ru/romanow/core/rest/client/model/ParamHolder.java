package ru.romanow.core.rest.client.model;

import com.google.common.base.MoreObjects;

/**
 * Created by ronin on 22.01.17
 */
public class ParamHolder {
    private Integer code;
    private String message;

    public ParamHolder() {}

    public ParamHolder(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {

        return code;
    }

    public ParamHolder setCode(Integer code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public ParamHolder setMessage(String message) {
        this.message = message;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("code", code)
                          .add("message", message)
                          .toString();
    }
}
