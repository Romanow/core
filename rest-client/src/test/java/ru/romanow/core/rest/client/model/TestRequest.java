package ru.romanow.core.rest.client.model;

import com.google.common.base.MoreObjects;

/**
 * Created by ronin on 13.02.16
 */
public class TestRequest {
    private String message;
    private String data;

    public TestRequest() {}

    public TestRequest(String message, String data) {
        this.message = message;
        this.data = data;
    }

    public String getMessage() {

        return message;
    }

    public TestRequest setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getData() {
        return data;
    }

    public TestRequest setData(String data) {
        this.data = data;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("message", message)
                          .add("data", data)
                          .toString();
    }
}
