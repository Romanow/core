package ru.romanow.core.rest.client;

import com.google.gson.JsonSyntaxException;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.romanow.core.rest.client.configuration.TestServerConfiguration;
import ru.romanow.core.rest.client.exception.HttpRestClientException;
import ru.romanow.core.rest.client.exception.HttpRestResourceException;
import ru.romanow.core.rest.client.exception.HttpRestServerException;
import ru.romanow.core.rest.client.exception.HttpRestTimeoutException;
import ru.romanow.core.rest.client.model.ErrorHolder;
import ru.romanow.core.rest.client.model.ParamHolder;
import ru.romanow.core.rest.client.model.TestRequest;
import ru.romanow.core.rest.client.model.TestResponse;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static ru.romanow.core.rest.client.web.TestController.*;

/**
 * Created by ronin on 12.02.16
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = TestServerConfiguration.class)
public class RestClientTest {

    private RestClient restClient;
    @Value("${local.server.port}")
    private Integer port;

    private String host;
    private String url;
    
    @Before
    public void setUp() {
        restClient = new RestClient();

        host = "http://127.0.0.1";
        url = host + ":" + port;
    }

    @Test
    public void testGet() {
        String response =
                restClient.get(url + PING_ENDPOINT, String.class)
                          .execute()
                          .get();

        assertEquals("OK", response);
    }

    @Test
    public void testGetWithParam() {
        Integer code = RandomUtils.nextInt(0, 100);
        String message = RandomStringUtils.randomAlphabetic(10);
        ParamHolder response =
                restClient.get(url + PARAM_ENDPOINT, ParamHolder.class)
                          .addParam("message", message)
                          .addParam("code", code.toString())
                          .execute()
                          .get();

        assertEquals(message, response.getMessage());
        assertEquals(code, response.getCode());
    }

    @Test
    public void testPostWithBody() {
        String message = RandomStringUtils.randomAlphanumeric(10);
        String data = RandomStringUtils.randomAlphabetic(100);
        TestRequest request = new TestRequest(message, data);

        TestResponse response =
                restClient.post(url + REQUEST_ENDPOINT, TestResponse.class)
                          .requestBody(request)
                          .execute()
                          .get();

        assertEquals(data.length(), response.getCode().intValue());
        assertEquals(message, response.getMessage());
    }

    @Test
    public void testClientRetry() {
        restClient.get(url + RETRY_REQUEST_ENDPOINT, String.class)
                  .requestProcessingTimeout(100, TimeUnit.SECONDS)
                  .retryCount(5)
                  .execute();
    }

    @Test(expected = HttpRestTimeoutException.class)
    public void testTimeoutThrow() {
        restClient.get(url + TIMEOUT_REQUEST_ENDPOINT, Void.class)
                  .requestProcessingTimeout(100, TimeUnit.MILLISECONDS)
                  .execute();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTimeoutMapping() {
        restClient.get(url + TIMEOUT_REQUEST_ENDPOINT, Void.class)
                  .requestProcessingTimeout(100, TimeUnit.MILLISECONDS)
                  .timeoutExceptionMapper(IllegalArgumentException::new)
                  .execute();
    }

    @Test
    public void testPostParsingException() {
        String data = RandomStringUtils.randomAlphanumeric(100);
        String message = RandomStringUtils.randomAlphabetic(10);
        TestRequest request = new TestRequest(message, data);

        try {
            restClient.post(url + REQUEST_ENDPOINT, String.class)
                      .requestBody(request)
                      .execute()
                      .get();
        } catch (Exception exception) {
            assertEquals(JsonSyntaxException.class, exception.getCause().getClass());
        }
    }

    @Test
    public void testError() {
        Integer code = RandomUtils.nextInt(0, 100);
        String message = RandomStringUtils.randomAlphabetic(10);
        try {
            restClient.get(url + ERROR_BODY_ENDPOINT, ParamHolder.class)
                      .addParam("message", message)
                      .addParam("code", code.toString())
                      .execute()
                      .get();

        } catch (Exception exception) {
            assertEquals(HttpRestServerException.class, exception.getClass());
            HttpRestServerException serverException = (HttpRestServerException)exception;
            assertNull(serverException.getBody());
        }
    }

    @Test
    public void testErrorBody() {
        Integer code = RandomUtils.nextInt(0, 100);
        String message = RandomStringUtils.randomAlphabetic(10);
        try {
            restClient.get(url + ERROR_BODY_ENDPOINT, ParamHolder.class)
                      .addParam("message", message)
                      .addParam("code", code.toString())
                      .addErrorResponseClass(HttpStatus.INTERNAL_SERVER_ERROR.value(), ErrorHolder.class)
                      .execute()
                      .get();

        } catch (Exception exception) {
            assertEquals(HttpRestServerException.class, exception.getClass());
            HttpRestServerException serverException = (HttpRestServerException)exception;
            ErrorHolder errorHolder = (ErrorHolder)serverException.getBody();
            assertEquals(message, errorHolder.getMessage());
            assertEquals(code, errorHolder.getCode());
        }
    }

    @Test
    public void testErrorBodyParsingException() {
        Integer code = RandomUtils.nextInt(0, 100);
        String message = RandomStringUtils.randomAlphabetic(10);
        try {
            restClient.get(url + ERROR_BODY_ENDPOINT, String.class)
                      .addParam("message", message)
                      .addParam("code", code.toString())
                      .addErrorResponseClass(HttpStatus.INTERNAL_SERVER_ERROR.value(), String.class)
                      .execute()
                      .get();

        } catch (Exception exception) {
            assertEquals(JsonSyntaxException.class, exception.getCause().getClass());
        }
    }

    @Test(expected = HttpRestClientException.class)
    public void testClientError() {
        restClient.get(url + BAD_REQUEST_ENDPOINT, Void.class)
                  .execute();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testClientErrorMapping() {
        restClient.get(url + BAD_REQUEST_ENDPOINT, Void.class)
                  .addExceptionMapping(HttpStatus.BAD_REQUEST.value(), IllegalArgumentException::new)
                  .execute();
    }

    @Test(expected = HttpRestClientException.class)
    public void testClientErrorThrow() {
        restClient.get(url + BAD_REQUEST_ENDPOINT, Void.class)
                  .execute();
    }

    @Test
    @Deprecated
    public void testClientErrorSuppress() {
        restClient.get(url + BAD_REQUEST_ENDPOINT, Void.class)
                  .addIgnoredErrors(HttpStatus.BAD_REQUEST.value())
                  .execute();
    }

    @Test(expected = HttpRestServerException.class)
    public void testServerErrorThrow() {
        restClient.get(url + INTERNAL_SERVER_ERROR_ENDPOINT, Void.class)
                  .execute();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testServerErrorMapping() {
        restClient.get(url + INTERNAL_SERVER_ERROR_ENDPOINT, Void.class)
                  .addExceptionMapping(HttpStatus.INTERNAL_SERVER_ERROR.value(), IllegalArgumentException::new)
                  .execute();
    }

    @Test
    @Deprecated
    public void testServerErrorSuppress() {
        restClient.get(url + INTERNAL_SERVER_ERROR_ENDPOINT, Void.class)
                  .addIgnoredErrors(HttpStatus.INTERNAL_SERVER_ERROR.value())
                  .execute();
    }

    @Test(expected = HttpRestResourceException.class)
    public void testResourceErrorThrow() {
        restClient.get(host + ":5501", Void.class)
                  .execute();
    }

    @Test(expected = HttpRestResourceException.class)
    public void testResourceError() {
        restClient.get(host + ":5501", Void.class)
                  .execute();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testResourceErrorMapping() {
        restClient.get(host + ":5501", Void.class)
                  .resourceExceptionMapper(IllegalArgumentException::new)
                  .execute();
    }

    @Test
    @Deprecated
    public void testResourceErrorSuppress() {
        restClient.get(host + ":5501", Void.class)
                  .processResourceExceptions(true)
                  .execute();
    }
}