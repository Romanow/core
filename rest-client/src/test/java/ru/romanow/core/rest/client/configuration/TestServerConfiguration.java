package ru.romanow.core.rest.client.configuration;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

/**
 * Created by ronin on 14.02.16
 */
@Configuration
@SpringBootApplication(scanBasePackages = "ru.romanow.core.rest")
public class TestServerConfiguration {}
