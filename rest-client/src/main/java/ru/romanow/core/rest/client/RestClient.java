package ru.romanow.core.rest.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.client.AsyncRestTemplate;
import ru.romanow.core.rest.client.exception.*;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Supplier;

/**
 * Created by ronin on 12.02.16
 */
public class RestClient {
    private static final Logger logger = LoggerFactory.getLogger(RestClient.class);

    private static final int DEFAULT_REQUEST_TIMEOUT = 3000;
    
    private final AsyncRestTemplate asyncRestTemplate;

    public RestClient() {
        this.asyncRestTemplate = new AsyncRestTemplate();
    }

    public RestClient(AsyncRestTemplate asyncRestTemplate) {
        this.asyncRestTemplate = asyncRestTemplate;
    }

    // region Builders
    public <RESP> GetRequestBuilder<RESP> get(@Nonnull String url, @Nonnull Class<RESP> responseClass) {
        return new GetRequestBuilder<>(url, responseClass);
    }

    public <RESP> PostRequestBuilder<RESP> post(String url, Class<RESP> requestClass) {
        return new PostRequestBuilder<>(url, requestClass);
    }
    // endregion

    public abstract class RequestBuilder<RESP, T extends RequestBuilder<RESP, T>> {
        protected String url;

        protected Class<RESP> responseClass;

        protected Supplier<Optional<RESP>> defaultResponse;
        protected Map<Integer, Class> errorResponseClass;

        protected boolean processClientExceptions;
        protected boolean processServerExceptions;
        protected Set<Integer> ignoredErrors;
        protected Map<Integer, ExceptionMapper<? extends RuntimeException, HttpStatusBasedException>> exceptionMapping;

        protected boolean processResourceExceptions;
        protected ExceptionMapper<? extends RuntimeException, HttpRestResourceException> resourceExceptionMapper;

        protected int requestProcessingTimeout;
        protected TimeUnit timeoutTimeUnit;
        protected int retryCount;
        protected boolean processTimeoutExceptions;
        protected TimeoutExceptionMapper<? extends RuntimeException> timeoutExceptionMapper;

        protected Map<String, Object> params;

        public RequestBuilder(@Nonnull String url, @Nonnull Class<RESP> responseClass) {
            Objects.requireNonNull(url);
            Objects.requireNonNull(responseClass);

            this.url = url;
            this.responseClass = responseClass;
            this.defaultResponse = Optional::empty;
            this.errorResponseClass = new HashMap<>();
            
            this.processClientExceptions = true;
            this.processServerExceptions = true;
            this.ignoredErrors = new HashSet<>();
            this.exceptionMapping = new HashMap<>();
            
            this.processResourceExceptions = true;
            
            this.requestProcessingTimeout = DEFAULT_REQUEST_TIMEOUT;
            this.timeoutTimeUnit = TimeUnit.MILLISECONDS;
            this.processTimeoutExceptions = true;
            this.retryCount = 0;

            this.params = new HashMap<>();
        }

        public T defaultResponse(Supplier<Optional<RESP>> defaultResponse) {
            this.defaultResponse = defaultResponse;

            return getThis();
        }

        public T addErrorResponseClass(Integer statusCode, Class<?> errorResponseClass) {
            this.errorResponseClass.put(statusCode, errorResponseClass);

            return getThis();
        }

        public T processClientExceptions(boolean process) {
            this.processClientExceptions = process;

            return getThis();
        }

        public T processServerExceptions(boolean process) {
            this.processServerExceptions = process;

            return getThis();
        }

        public T addIgnoredErrors(Integer ... statusCodes) {
            this.ignoredErrors.addAll(Arrays.asList(statusCodes));

            return getThis();
        }

        public T addExceptionMapping(
                Integer statusCode, ExceptionMapper<? extends RuntimeException, HttpStatusBasedException> exceptionMapper) {
            this.exceptionMapping.put(statusCode, exceptionMapper);
            return getThis();
        }

        public T processResourceExceptions(boolean processResourceExceptions) {
            this.processResourceExceptions = processResourceExceptions;
            return getThis();
        }

        public T resourceExceptionMapper(
                ExceptionMapper<? extends RuntimeException, HttpRestResourceException> resourceExceptionMapper) {
            this.resourceExceptionMapper = resourceExceptionMapper;
            return getThis();
        }

        public T requestProcessingTimeout(int requestProcessingTimeout, TimeUnit timeoutTimeUnit) {
            this.requestProcessingTimeout = requestProcessingTimeout;
            this.timeoutTimeUnit = timeoutTimeUnit;

            return getThis();
        }

        public T retryCount(int retryCount) {
            this.retryCount = retryCount;

            return getThis();
        }
        public T processTimeoutExceptions(boolean processTimeoutExceptions) {
            this.processTimeoutExceptions = processTimeoutExceptions;
            return getThis();
        }

        public T timeoutExceptionMapper(
                TimeoutExceptionMapper<? extends RuntimeException> timeoutExceptionMapper) {
            this.timeoutExceptionMapper = timeoutExceptionMapper;
            return getThis();
        }

        public T addParam(String name, Object value) {
            this.params.put(name, value);

            return getThis();
        }

        public T addParams(Map<String, Object> params) {
            this.params = params;

            return getThis();
        }

        public Optional<RESP> execute() {
            return execute(retryCount);
        }

        protected Optional<RESP> execute(int retries) {
            Optional<RESP> result = defaultResponse.get();
            ListenableFuture<ResponseEntity<RESP>> future = makeRequest();
            try {
                ResponseEntity<RESP> response = future.get(requestProcessingTimeout, timeoutTimeUnit);
                HttpStatus status = response.getStatusCode();
                int statusCode = status.value();

                if (status.is2xxSuccessful()) {
                    result = Optional.ofNullable(response.getBody());
                } else if (status.is4xxClientError() || status.is5xxServerError()) {
                    response.getBody();
                    Formatter formatter = new Formatter();
                    String errorMessage = formatter
                            .format("Request [%s] failed with error: %d:%s", url, statusCode, status.getReasonPhrase())
                            .toString();

                    logger.warn(errorMessage);

                    boolean processError = status.is4xxClientError() && processClientExceptions ||
                            status.is5xxServerError() && processServerExceptions;
                    if (processError && !ignoredErrors.contains(status.value())) {
                        HttpStatusBasedException restException =
                                getStatusBasedException(status, status.getReasonPhrase(), response.getBody());
                        if (exceptionMapping.containsKey(statusCode)) {
                            throw exceptionMapping.get(statusCode).produce(restException);
                        } else {
                            throw restException;
                        }
                    }
                }
            } catch (ExecutionException exception) {
                Formatter formatter = new Formatter();
                String errorMessage = formatter
                        .format("Request [%s] failed with error: %s",
                                url, exception.getMessage()).toString();

                logger.warn(errorMessage);

                if (!processResourceExceptions) {
                    HttpRestResourceException resourceException =
                            new HttpRestResourceException(exception.getCause());
                    if (resourceExceptionMapper != null) {
                        throw resourceExceptionMapper.produce(resourceException);
                    } else {
                        throw resourceException;
                    }
                }
            } catch (TimeoutException exception) {
                Formatter formatter =  new Formatter();
                String errorMessage = formatter
                        .format("Request [%s] failed by timeout exception", url).toString();
                logger.warn(errorMessage);

                if (processTimeoutExceptions) {
                    if (retries > 0) {
                        execute(retries - 1);
                    } else {
                        if (timeoutExceptionMapper != null) {
                            throw timeoutExceptionMapper.produce(exception);
                        } else {
                            throw new HttpRestTimeoutException(exception);
                        }
                    }
                }
            } catch (InterruptedException exception) {
                logger.error("Request {} interrupted", url);
            }

            return result;
        }

        protected abstract ListenableFuture<ResponseEntity<RESP>> makeRequest();

        protected abstract T getThis();

        private HttpStatusBasedException getStatusBasedException(HttpStatus status, String reasonPhrase, Object responseObject) {
            if (status.is4xxClientError()) {
                return new HttpRestClientException(status.value(), reasonPhrase, responseObject);
            } else {
                return new HttpRestServerException(status.value(), reasonPhrase, responseObject);
            }
        }
    }

    // region Get builder
    public class GetRequestBuilder<RESP>
            extends RequestBuilder<RESP, GetRequestBuilder<RESP>> {

        public GetRequestBuilder(String url, Class<RESP> requestClass) {
            super(url, requestClass);
        }

        @Override
        protected ListenableFuture<ResponseEntity<RESP>> makeRequest() {
            return asyncRestTemplate.getForEntity(url, responseClass, params);
        }

        @Override
        protected GetRequestBuilder<RESP> getThis() {
            return this;
        }
    }
    // endregion

    // region Post builder
    public class PostRequestBuilder<RESP>
            extends RequestBuilder<RESP, PostRequestBuilder<RESP>> {

        private Object requestBody;

        public PostRequestBuilder(String url, Class<RESP> requestClass) {
            super(url, requestClass);
        }

        public PostRequestBuilder<RESP> requestBody(Object requestBody) {
            this.requestBody = requestBody;
            return getThis();
        }

        @Override
        protected ListenableFuture<ResponseEntity<RESP>> makeRequest() {
            return asyncRestTemplate.postForEntity(url, new HttpEntity<>(requestBody), responseClass);
        }

        @Override
        protected PostRequestBuilder<RESP> getThis() {
            return this;
        }
    }
    // endregion
}
